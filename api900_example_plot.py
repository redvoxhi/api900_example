"""
This file contains the plot function definitions for the api900 example modules
"""

import numpy as np
import matplotlib.pyplot as plt


def plot_wf(station_name: str,
            time: np.ndarray, time_zero: float, time_units: str,
            wf: np.ndarray, channel_type: str, channel_units: str):
    """
    Plot single component time series
    :param station_name: Station ID for plot title
    :param time: time
    :param time_zero: start time of record, gets mapped to zero
    :param time_units: time units string for plot label
    :param wf: waveform
    :param channel_type: channel description string for plot title
    :param channel_units: channel units string for plot label
    :return: plot to screen
    """
    plt.figure()
    plt.plot(time-time_zero, wf)
    plt.ylabel(channel_units)
    plt.title(channel_type + " " + station_name)
    plt.ylabel(channel_units)
    plt.xlabel(time_units)


def plot_wf_3c(station_name: str,
               time: np.ndarray, time_zero: float, time_units: str,
               wfx: np.ndarray, wfy: np.ndarray, wfz: np.ndarray,
               channel_type: str, channel_units: str):
    """
    Plot three-component (3C) time series
    :param station_name: Station ID for plot title
    :param time: time
    :param time_zero: start time of record, gets mapped to zero
    :param time_units: time units string for plot label
    :param wfx: waveform along x sensor axis
    :param wfy: waveform along y sensor axis
    :param wfz: waveform along z sensor axis
    :param channel_type: channel description string for plot title
    :param channel_units: channel units string for plot label
    :return: plot to screen
    """
    plt.figure()
    plt.subplot(311), plt.plot(time-time_zero, wfx)
    plt.ylabel(channel_units)
    plt.title(channel_type + " " + station_name)
    plt.subplot(312), plt.plot(time-time_zero, wfy)
    plt.ylabel(channel_units)
    plt.subplot(313), plt.plot(time-time_zero, wfz)
    plt.ylabel(channel_units)
    plt.xlabel(time_units)
