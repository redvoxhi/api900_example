# Redvox API 900 Examples #

This repository contains examples of how to use the RedVox SDK to load Redvox API 900 data.

You will need an installation of Python and the RedVox SDK to run these examples.

Please refer to [redvox documentation](https://bitbucket.org/redvoxhi/redvox-api900-python-reader/src/master/docs/v2.9.10/redvox-api900-docs.md) for information on how to install Python and the RedVox SDK.

### How do I view/run the examples? ###

If you want to run the examples on your own, download the repository.

The data used in this example is from the SpaceX Falcon 9 Launch, Dragon Explosion on January 19, 2020.

Use the link to view the report and download the data: http://redvox.io/@/f2e1

Once you have downloaded the data, update the corresponding variables in the example python files to point to the downloaded data, then run the files.

* [Loading a single file](https://bitbucket.org/redvoxhi/api900_example/src/master/api900_example_00_load_single.py): update the INPUT_FILE variable
* [Loading all the files](https://bitbucket.org/redvoxhi/api900_example/src/master/api900_example_01_load_all.py): update the INPUT_DIR variable
* [Loading a range of files](https://bitbucket.org/redvoxhi/api900_example/src/master/api900_example_02_load_range.py): update the INPUT_DIR variable

If you want to take a visual tour of the SDK, read through the demos.

* [Loading a single file](https://redvoxhi.bitbucket.io/api900_examples/ex_00_load_single_file.html)
* [Loading all the files](https://redvoxhi.bitbucket.io/api900_examples/ex_01_load_all_files.html)
* [Loading a range of files](https://redvoxhi.bitbucket.io/api900_examples/ex_02_load_range.html)
