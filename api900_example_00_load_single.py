"""
This example shows how to load Redvox api900 data from a single file
You do not need any prior knowledge of the redvox sdk to run this example
You MUST change the input file path to match where you downloaded the data to
"""
import numpy as np
import redvox.api900.reader as reader
import matplotlib.pyplot as plt

from api900_example_plot import plot_wf_3c, plot_wf

# You MUST change this to match the location of the downloaded data files
INPUT_FILE = "f2e1/api900/2020/01/19/1637681011_1579447801440.rdvxz"
# You may change this value to adjust how much information is displayed:
# 1 = print to screen, 2 = print and plot to screen
VERBOSITY = 2


def main(input_file: str, verbosity: int):
    """
    This example loads and plots time series data from a single file
    :param input_file: file to load
    :param verbosity: 1 = print to screen, 2 = print and plot to screen
    :return:
    """

    # No prior knowledge needed, loads and plots everything:
    wrapped_redvox_packets = reader.read_rdvxz_file(input_file)

    if not wrapped_redvox_packets:
        print('*** No available data, check the directory path or file name ***')
        return

    # Get the station id
    station_id_str = wrapped_redvox_packets.redvox_id()
    zero_time_epoch_s = wrapped_redvox_packets.app_file_start_timestamp_machine()/1E6

    # MICROPHONE SENSOR
    if wrapped_redvox_packets.has_microphone_sensor():
        mic_is_scrambled = wrapped_redvox_packets.is_scrambled()
        mic_sample_rate_hz = wrapped_redvox_packets.microphone_sensor().sample_rate_hz()
        mic_payloads = wrapped_redvox_packets.microphone_sensor().payload_values()
        mic_wf = mic_payloads.astype(np.float)
        mic_time_s = np.arange(len(mic_wf))/mic_sample_rate_hz
        mic_time_epoch_s = zero_time_epoch_s + mic_time_s

        if verbosity > 0:
            print("\nStation ID:", station_id_str)
            print("Make and model:", wrapped_redvox_packets.device_make() + " "
                  + wrapped_redvox_packets.device_model())
            print("Operating System: ", wrapped_redvox_packets.device_os() + " "
                  + wrapped_redvox_packets.device_os_version())
            print("App version: " "RedVox " + wrapped_redvox_packets.app_version())
            print("mic scrambled?: ", mic_is_scrambled)

        if verbosity > 1:
            plot_wf(station_name=station_id_str,
                    time=mic_time_epoch_s, time_zero=zero_time_epoch_s, time_units='Time, s',
                    wf=mic_wf, channel_type='Microphone', channel_units='Counts')

    # BAROMETER SENSOR
    if wrapped_redvox_packets.has_barometer_sensor():
        bar_wf = wrapped_redvox_packets.barometer_sensor().payload_values()
        bar_timestamps = wrapped_redvox_packets.barometer_sensor().timestamps_microseconds_utc()
        bar_time_s = bar_timestamps/1E6

        if verbosity > 1:
            plot_wf(station_name=station_id_str,
                    time=bar_time_s, time_zero=zero_time_epoch_s, time_units='Time, s',
                    wf=bar_wf, channel_type='Barometer', channel_units='kPa')

    # ACCELEROMETER 3C SENSOR
    if wrapped_redvox_packets.has_accelerometer_sensor():
        acc_wf_x = wrapped_redvox_packets.accelerometer_sensor().payload_values_x()
        acc_wf_y = wrapped_redvox_packets.accelerometer_sensor().payload_values_y()
        acc_wf_z = wrapped_redvox_packets.accelerometer_sensor().payload_values_z()
        acc_timestamps = wrapped_redvox_packets.accelerometer_sensor().timestamps_microseconds_utc()
        acc_time_s = acc_timestamps/1E6

        if verbosity > 1:
            plot_wf_3c(station_name=station_id_str,
                       time=acc_time_s, time_zero=zero_time_epoch_s, time_units='Time, s',
                       wfx=acc_wf_x, wfy=acc_wf_y, wfz=acc_wf_z,
                       channel_type='Accelerometer', channel_units='m/s^2')
        plt.show()


if __name__ == "__main__":
    # Data Source URL: http://redvox.io/@/f2e1
    # Change the input file path and verbosity values above as needed
    file_name = INPUT_FILE
    main(file_name, verbosity=VERBOSITY)
