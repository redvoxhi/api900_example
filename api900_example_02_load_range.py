"""
This example shows how to load a range of Redvox api900 data from directory
You do not need any prior knowledge of the redvox sdk to run this example
You MUST change the input directory path to match where you downloaded the data to
"""
import numpy as np
import redvox.api900.reader as reader
import matplotlib.pyplot as plt

from api900_example_plot import plot_wf_3c, plot_wf

# You MUST change this to match the location of the downloaded data files
INPUT_DIR = "f2e1/api900/"
# You may change this value to adjust how much information is displayed:
# 1 = print to screen, 2 = print and plot to screen
VERBOSITY = 2
# You may change this value to adjust which stations are reported:
# If empty, plots ALL the stations in the directory
STATIONS = []
# You may change these values to adjust the start and end times being reported:
# timestamps are in seconds since UTC epoch
EPISODE_START_EPOCH_S = 1579447800 + 200
EPISODE_END_EPOCH_S = 1579448160 - 100


def main(input_dir: str, episode_start_s: float, episode_end_s: float, stations: list, verbosity: int):
    """
    This example file loads and plots time series data
    :param input_dir: directory of the data with RedVox api900 directory and file structure
    :param episode_start_s: start unix time UTC in seconds
    :param episode_end_s: end unix time UTC in seconds
    :param stations: list of station IDs
    :param verbosity: 1 = print to screen, 2 = print and plot to screen
    :return:
    """

    grouped_packets = reader.read_rdvxz_file_range(directory=input_dir,
                                                   start_timestamp_utc_s=int(episode_start_s),
                                                   end_timestamp_utc_s=int(episode_end_s),
                                                   redvox_ids=stations,
                                                   structured_layout=True,
                                                   concat_continuous_segments=True)

    if not grouped_packets:
        print('*** No available data, check the times or directory path ***')
        return

    for redvox_ids, wrapped_redvox_packets in grouped_packets.items():
        # Get the station id from the first packet
        station_id_str = wrapped_redvox_packets[0].redvox_id()
        zero_time_epoch_s = wrapped_redvox_packets[0].app_file_start_timestamp_machine()/1E6

        # MICROPHONE SENSOR
        if wrapped_redvox_packets[0].has_microphone_sensor():
            mic_is_scrambled = wrapped_redvox_packets[0].is_scrambled()
            channel_mic_0 = wrapped_redvox_packets[0].microphone_sensor()
            # Assumes sample rate is constant during the record
            mic_sample_rate_hz = channel_mic_0.sample_rate_hz()

            mic_payloads = \
                list(map(lambda packet: packet.microphone_sensor().payload_values(), wrapped_redvox_packets))

            mic_wf = np.concatenate(mic_payloads).astype(np.float)
            mic_time_s = np.arange(len(mic_wf))/mic_sample_rate_hz
            mic_time_epoch_s = zero_time_epoch_s + mic_time_s

            if verbosity > 0:
                print("\nFull Station ID:", redvox_ids)
                print("Short Station ID:", station_id_str)
                print("Make and model:", wrapped_redvox_packets[0].device_make() + " "
                      + wrapped_redvox_packets[0].device_model())
                print("Operating System: ", wrapped_redvox_packets[0].device_os() + " "
                      + wrapped_redvox_packets[0].device_os_version())
                print("App version: " "RedVox " + wrapped_redvox_packets[0].app_version())
                print("mic scrambled?: ", mic_is_scrambled)

            if verbosity > 1:
                plot_wf(station_name=station_id_str,
                        time=mic_time_epoch_s, time_zero=zero_time_epoch_s, time_units='Time, s',
                        wf=mic_wf, channel_type='Microphone', channel_units='Counts')

        # BAROMETER SENSOR
        if wrapped_redvox_packets[0].has_barometer_sensor():
            bar_payloads = \
                list(map(lambda packet: packet.barometer_sensor().payload_values(),
                         wrapped_redvox_packets))
            bar_timestamps = \
                list(map(lambda packet: packet.barometer_sensor().timestamps_microseconds_utc(),
                         wrapped_redvox_packets))
            bar_wf = np.concatenate(bar_payloads)
            bar_time_s = np.concatenate(bar_timestamps)/1E6

            if verbosity > 1:
                plot_wf(station_name=station_id_str,
                        time=bar_time_s, time_zero=zero_time_epoch_s, time_units='Time, s',
                        wf=bar_wf, channel_type='Barometer', channel_units='kPa')

        # ACCELEROMETER 3C SENSOR
        if wrapped_redvox_packets[0].has_accelerometer_sensor():
            acc_payloads_x = \
                list(map(lambda packet: packet.accelerometer_sensor().payload_values_x(),
                         wrapped_redvox_packets))
            acc_payloads_y = \
                list(map(lambda packet: packet.accelerometer_sensor().payload_values_y(),
                         wrapped_redvox_packets))
            acc_payloads_z = \
                list(map(lambda packet: packet.accelerometer_sensor().payload_values_z(),
                         wrapped_redvox_packets))
            acc_timestamps = \
                list(map(lambda packet: packet.accelerometer_sensor().timestamps_microseconds_utc(),
                         wrapped_redvox_packets))
            acc_wf_x = np.concatenate(acc_payloads_x)
            acc_wf_y = np.concatenate(acc_payloads_y)
            acc_wf_z = np.concatenate(acc_payloads_z)
            acc_time_s = np.concatenate(acc_timestamps)/1E6

            if verbosity > 1:
                plot_wf_3c(station_name=station_id_str,
                           time=acc_time_s, time_zero=zero_time_epoch_s, time_units='Time, s',
                           wfx=acc_wf_x, wfy=acc_wf_y, wfz=acc_wf_z,
                           channel_type='Accelerometer', channel_units='m/s^2')

        # GYROSCOPE 3C SENSOR
        if wrapped_redvox_packets[0].has_gyroscope_sensor():
            gyr_payloads_x = \
                list(map(lambda packet: packet.gyroscope_sensor().payload_values_x(),
                         wrapped_redvox_packets))
            gyr_payloads_y = \
                list(map(lambda packet: packet.gyroscope_sensor().payload_values_y(),
                         wrapped_redvox_packets))
            gyr_payloads_z = \
                list(map(lambda packet: packet.gyroscope_sensor().payload_values_z(),
                         wrapped_redvox_packets))
            gyr_timestamps = \
                list(map(lambda packet: packet.gyroscope_sensor().timestamps_microseconds_utc(),
                         wrapped_redvox_packets))
            gyr_wf_x = np.concatenate(gyr_payloads_x)
            gyr_wf_y = np.concatenate(gyr_payloads_y)
            gyr_wf_z = np.concatenate(gyr_payloads_z)
            gyr_time_s = np.concatenate(gyr_timestamps)/1E6

            if verbosity > 1:
                plot_wf_3c(station_name=station_id_str,
                           time=gyr_time_s, time_zero=zero_time_epoch_s, time_units='Time, s',
                           wfx=gyr_wf_x, wfy=gyr_wf_y, wfz=gyr_wf_z,
                           channel_type='Gyroscope', channel_units='rad/s')

        # MAGNETOMETER 3C SENSOR
        if wrapped_redvox_packets[0].has_magnetometer_sensor():
            mag_payloads_x = \
                list(map(lambda packet: packet.magnetometer_sensor().payload_values_x(),
                         wrapped_redvox_packets))
            mag_payloads_y = \
                list(map(lambda packet: packet.magnetometer_sensor().payload_values_y(),
                         wrapped_redvox_packets))
            mag_payloads_z = \
                list(map(lambda packet: packet.magnetometer_sensor().payload_values_z(),
                         wrapped_redvox_packets))
            mag_timestamps = \
                list(map(lambda packet: packet.magnetometer_sensor().timestamps_microseconds_utc(),
                         wrapped_redvox_packets))
            mag_wf_x = np.concatenate(mag_payloads_x)
            mag_wf_y = np.concatenate(mag_payloads_y)
            mag_wf_z = np.concatenate(mag_payloads_z)
            mag_time_s = np.concatenate(mag_timestamps)/1E6

            if verbosity > 1:
                plot_wf_3c(station_name=station_id_str,
                           time=mag_time_s, time_zero=zero_time_epoch_s, time_units='Time, s',
                           wfx=mag_wf_x, wfy=mag_wf_y, wfz=mag_wf_z,
                           channel_type='Magnetometer', channel_units='microTesla')

        # LUMINOSITY SENSOR
        if wrapped_redvox_packets[0].has_light_sensor():
            lux_payloads = \
                list(map(lambda packet: packet.light_sensor().payload_values(),
                         wrapped_redvox_packets))
            lux_timestamps = \
                list(map(lambda packet: packet.light_sensor().timestamps_microseconds_utc(),
                         wrapped_redvox_packets))
            lux_wf = np.concatenate(lux_payloads)
            lux_time_s = np.concatenate(lux_timestamps)/1E6

            if verbosity > 1:
                plot_wf(station_name=station_id_str,
                        time=lux_time_s, time_zero=zero_time_epoch_s, time_units='Time, s',
                        wf=lux_wf, channel_type='Luminosity', channel_units='lux')
        plt.show()


if __name__ == "__main__":
    # Data Source URL: http://redvox.io/@/f2e1
    # Change the input parameters above as needed
    data_dir = INPUT_DIR
    station_ids = STATIONS
    epoch_start_s = EPISODE_START_EPOCH_S
    epoch_end_s = EPISODE_END_EPOCH_S

    main(data_dir, epoch_start_s, epoch_end_s, station_ids, verbosity=VERBOSITY)
